<?php
/**
 * Created by PhpStorm.
 * User: hasterwilliam
 * Date: 3/20/18
 * Time: 4:42 PM
 */

namespace App\Providers;


use Illuminate\Support\Facades\Redis;
use Illuminate\Support\ServiceProvider;

class CacheServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('redis', function () {
            return new Redis();
        });
    }
}